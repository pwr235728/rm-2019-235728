import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch

from math import *

delta = 0.05
x_size = 10
y_size = 10
obst_vect = [(-4, 5), (-2, -5), (5, 2), (3, -3)]
start_point = (-10, -3)
finish_point = (10, 3)

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = X + Y

D0 = 100
Kp = 0.02
Ko = 10

zmax = 100

# Funckja licząca kwadrat odległość pomiedzy punktami
def dist2(p1, p2):
    dx = p1[0] - p2[0]
    dy = p1[1] - p2[1]
    return dx**2 + dy**2


# Funckja licząca odległość pomiedzy punktami
def dist(p1, p2):
    return np.sqrt(dist2(p1, p2))


# funkcja licząca wektor z podanych punktów
def vec2(p1, p2):
    return (p1[0]-p2[0], p1[1]-p2[1])


# Funkcja opisująca potencjał przyciągający
def up(qr, qk):
    dist2 = uist2(qr, qk)
    return 0.5*Kp*dist2


# Funkcja licząca siłę przyciągająca
def fp(qr, qk):
    return Kp*dist(qr, qk)


# Funkcja opisująca potencjał odpychajacy
def vo(qr, qo):
    d = dist(qr, qo)
    if d > D0:
        return 0
    else:
        return 0.5*Ko*(1/d - 1/D0)**2


# Funkcja licząca  siłę odpychającą
def fo(qr, qo):
    d = dist(qr, qo)
    if d > D0:
        return 0
    else:
        v = Ko*(1/d - 1/D0)*(1/(d**2))
        return v

# Funkcja wyliczająca sumę wszystkich sił odpychających
def fo_vec(qr, obst_v):
    f = 0
    for obst in obst_v:
        f += fo(qr, obst)
    return f

# Funkcja wyliczając wartość siły dla podanego punktu qr
def map_p(qr, qk, obst_v):
    return fo_vec(qr, obst_v) + fp(qr, qk)




for i, iv in enumerate(X):
    for j, jv in enumerate(Y):
        px = iv[i]
        py = jv[j]

        Z[j][i] = map_p((px, py), finish_point, obst_vect)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=0)

plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    pass  # plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
