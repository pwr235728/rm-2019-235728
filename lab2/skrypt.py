import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch

from math import *

delta = 0.05
x_size = 10
y_size = 10
obst_vect = [(-4, 5), (-2, -5), (5, 2), (3, -3)]
start_point = (-10, -3)
finish_point = (10, 3)

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = X + Y

D0 = 3
DR = D0
Kp = 0.025
Ko = 20

zmax = 100

# Funckja licząca kwadrat odległość pomiedzy punktami
def dist2(p1, p2):
    dx = p1[0] - p2[0]
    dy = p1[1] - p2[1]
    return dx**2 + dy**2


# Funckja licząca odległość pomiedzy punktami
def dist(p1, p2):
    return np.sqrt(dist2(p1, p2))


# funkcja licząca wektor z podanych punktów
def vec2(p1, p2):
    v = (p2[0]-p1[0], p2[1]-p1[1])
    return v

def len(v):
    return np.sqrt(v[0]**2 + v[1]**2)


def norm(v):
    l = len(v)
    v = (v[0]/l, v[1]/l)
    return v

# Funkcja opisująca potencjał przyciągający
def up(qr, qk):
    d2 = dist2(qr, qk)
    return 0.5*Kp*d2


# Funkcja licząca siłę przyciągająca
def fp(qr, qk):
    return Kp*dist(qr, qk)


# Funkcja opisująca potencjał odpychajacy
def vo(qr, qo):
    d = dist(qr, qo)
    if d > D0:
        return 0
    else:
        return 0.5*Ko*(1/d - 1/D0)**2


# Funkcja licząca  siłę odpychającą
def fo(qr, qo):
    d = dist(qr, qo)
    if d > D0:
        return 0
    else:
        v = Ko*(1/d - 1/D0)*(1/(d**2))
        return v

# Funkcja wyliczająca sumę wszystkich sił odpychających
def fo_vec(qr, obst_v):
    f = 0
    for obst in obst_v:
        f += fo(qr, obst)
    return f

# Funkcja wyliczając wartość siły dla podanego punktu qr
def map_p(qr, qk, obst_v):
    return fo_vec(qr, obst_v) + fp(qr, qk)



def vec_mul(v, f):
    vr = (v[0]*f, v[1]*f)
    return vr

def vec_sum(v1, v2):
    v = (v1[0]+v2[0], v1[1]+v2[1])
    return v


for i, iv in enumerate(X):
    for j, jv in enumerate(Y):
        px = iv[i]
        py = jv[j]

        Z[j][i] = map_p((px, py), finish_point, obst_vect)

path = []

p = start_point
i = 3000
while i > 0:
    i = i-1
    if -0.1 < p[0]-finish_point[0] < 0.1:
        if -0.1 < p[1]-finish_point[1] < 0.1:
            break

    k = finish_point

    # wektor przyciagajacy
    vp_f = fp(p,k)
    vp = vec2(p, k)
    vp = norm(vp)
    vp = vec_mul(vp, vp_f)
    # wektor odpychajacy - suma
    vo_s = (0,0)

    # sily odpychajace
    for obst in obst_vect:
        vo_f = fo(p, obst)
        vo = vec2(p, obst)

        if len(vo) > DR:
            vo_f = 0

        vo = norm(vo)
        vo = vec_mul(vo, vo_f)
        vo_s = vec_sum(vo_s, vo)

    v_dir = norm(vec_sum(vp, vec_mul(vo_s, -1)))


    # przyciagniecie wektora do siatki
    dx = round(v_dir[0])
    dy = round(v_dir[1])

    p = (p[0]+dx/20, p[1]+dy/20)
    px = p[0]
    py = p[1]

    # ograniczenie zeby nie wyszedl poza "mape"
    px = min(10, px)
    px = max(-10, px)
    py = min(10, py)
    py = max(-10, py)

    p = (px, py)
    path.append(p)

print(i)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=1, vmin=0)

plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

for p in path:
    plt.plot(p[0], p[1], "or", color='black', markersize=3)
for p in path:
    plt.plot(p[0], p[1], "or", color='magenta', markersize=1.5)
plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
