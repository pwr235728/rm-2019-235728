import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.patches as patches
import matplotlib.animation as animation
from matplotlib.path import Path
from scipy.spatial import distance

def add_obstacle(x, y, width, height):
    for i in np.arange(_scene.shape[0]):
        for j in np.arange(_scene.shape[1]):
            if j >= x and j <= x + width and i >= y and i <= y + height:
                _scene[j][i] = -1

def add_position(x,y):
    if x < 0 or x >= x_size or y < 0 or y >= y_size or scene[x][y] == -1:
        return #        raise Exception("Wjechales w przeszkode: (" + str(x) + "," + str(y) + ")")
    global x_pos
    global y_pos
    positions[0].append(x)
    positions[1].append(y)
    scene[x][y] = 1
    x_pos = x
    y_pos = y

def move_x(val):
    move(val, 1, 0)


def move_y(val):
    move(val, 0, 1)

def move(val, x, y):
    for i in np.arange(0, abs(val)):
        if val > 0:
            add_position(x_pos + x, y_pos + y)
        else:
            add_position(x_pos - x, y_pos - y)

def update(i):
    line.set_data([x+0.5 for x in positions[0][0:i]], [y+0.5 for y in positions[1][0:i]])
    return line

def coverage():
    full_size = len(scene.flatten())
    obst_size = np.count_nonzero(scene == -1)
    cleaned_size = np.count_nonzero(scene == 1)
    dirty_size = np.count_nonzero(scene == 0)
    coveraged_size = cleaned_size/(full_size - obst_size) * 100

    print("Koncowe polozenie: (" + str(x_pos) + ", " + str(y_pos)+ ")")
    print("Pelna scena: " + str(full_size))
    print("Przeszkody: " + str(obst_size))
    print("Posprzatane pole: " + str(cleaned_size))
    print("Zasmiecone pole: " + str(dirty_size))
    print("Pokrycie: " + str(round(coveraged_size,2)) + "%")

delta = 1
x_size = 40
y_size = 40
x_pos = 0
y_pos = 25
positions=[[],[]]
x = y = np.arange(0, 40.0, delta)
X, Y = np.meshgrid(x, y)
_scene = X*0
_scene[0][25]= 1 #punkt poczatkowy
add_obstacle(0,0,4,16)
add_obstacle(25,32,15,8)
add_obstacle(25,12,4,12)
scene = _scene.copy()
add_position(x_pos, y_pos)


# Uzupelnic kodem
animacja=False

dir_x = 0
dir_y = 1

def is_collision(pos, scene):
    #print(pos)
    if pos[0]+dir_x >= x_size or pos[1]+dir_y>= y_size:        
        return True
    if pos[0]+dir_x <0 or pos[1]+dir_y <0 or scene[pos[0]+dir_x][pos[1]+dir_y] == -1:
        return True
    return False

def is_coll():
    return is_collision((x_pos, y_pos), scene)

dir = 0 # 0 - up, 1- right, 2- down, 3-left


def forward():
    move_x(dir_x)
    move_y(dir_y)
    
def backward():
    move_x(-dir_x)
    move_y(-dir_y)
    
def right():
    global dir
    global dir_x
    global dir_y
    if dir == 0:
        dir_x=1
        dir_y=0
        
    if dir == 1:
        dir_x= 0
        dir_y=-1

    if dir == 2:
        dir_x= -1
        dir_y=0

    if dir == 3:
        dir_x=0
        dir_y=1        

    dir += 1
    dir %= 4

def left():
    right()
    right()
    right()

counter = 8100
cnt = 0
bl = 0
while counter > 0:
    counter -= 1

    if not is_coll():        
        forward()
        continue

    # kolizja

    if cnt%2 == 1:
        right()
        if bl < 2:
            cnt+=1
        else:
            bl =0
        if not is_coll():        
            forward()
        else:
            bl += 1
        right()
    else:
        left()
        if bl < 2:
            cnt+=1
        else:
            bl =-1
        if not is_coll():        
            forward()
        else:
            bl += 1
        left()


'''
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)
move_y(14)
move_x(1)
move_y(-14)
move_x(1)

move_y(14)
'''

#######################

_scene[x_pos][y_pos] = 1 
_scene = np.where(scene == 0, -0.5, _scene)

fig = plt.figure()
ax = fig.add_subplot(111)
line, =ax.plot([], [], linewidth=3)
plt.imshow(_scene.transpose(), cmap=cm.RdYlGn, origin='lower', extent=[0, x_size, 0, y_size])
ax.set_title("‘S’ shape pattern algorithm")
plt.grid(True)
if animacja:
    ani = animation.FuncAnimation(fig, update, interval=2,  save_count=1)
else:
    plt.plot(positions[0], positions[1], linewidth=3)
coverage()

plt.show()


